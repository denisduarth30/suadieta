import 'package:flutter/material.dart';

const labelTextStyle = {
  "white": TextStyle(
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w600,
      fontSize: 25,
      color: Colors.white),
  
  "black": TextStyle(
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w600,
      fontSize: 25,
      color: Colors.black),
};
